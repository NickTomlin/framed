// EWWW
var bean = require('bean');
window.bean = bean;

window.extractQueryString = function (href) {
  var queryIndex = href.indexOf('?');

  if (queryIndex < 0) return;

  var querystring = href.slice(queryIndex + 1);

  var data = {};
  var parts = querystring.split('&');

  for (var i = 0, l = parts.length; i < l; i ++) {
    var pair = parts[i].split('=');
    data[pair[0]] = pair[1] ? pair[1] : true;
  }

  return data;
};

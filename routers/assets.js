'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var assetsRouter = express.Router();


assetsRouter.post('/results', bodyParser(), function (req, res){
  var nonce = req.query.nonce;
  res.send("<html><title>Result</title><body><script>window.parent.postMessage('" + nonce + "', '*');</script></body></html>");
});

assetsRouter.get('/js/:filename', function (req, res) {
  var dir = path.resolve('public/js');
  var jsfile = req.params.filename.replace(/\.js/g, '') + '.js';

  res.sendfile([dir, jsfile].join('/'));
});

assetsRouter.get('/:filename', function (req, res) {
  res.sendfile(path.resolve('iframe-public/' + req.params.filename + '.html'));
});

module.exports = assetsRouter;
